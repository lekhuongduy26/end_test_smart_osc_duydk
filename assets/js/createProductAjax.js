function createXMLHttpRequestObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xmlhttp;
}

var sizes = [], colors = [], categories = [], name, price, description, quantity, image;
function resetFrom() {
    sizes = [];
    colors = [];
    categories = [];
    name = '';
    price = '';
    description = '';
    quantity = '';
    image = '';
}
var nameValidator = /^[A-Za-z0-9_.\-\s]{3,}$/;
var priceValidator= /[+-]?([0-9]*[.])?[0-9]+$/;
var descriptionValidator= /^[A-Za-z0-9_.\-\s]{3,}$/;
var quantityValidator= /^[0-9]+$/;
var imageValidator= /\.(gif|jpg|jpeg|png)$/i;
function validate() {
    let i = 0;

    if (!nameValidator.test(name)) {
        // console.log('name error');
        document.getElementById('nameError').innerHTML = "Name is invalid";
        i++;
    } else {
        document.getElementById('nameError').innerHTML = "";
    }

    if (!priceValidator.test(price)) {
        // console.log('price error');
        document.getElementById('priceError').innerHTML = "Price is invalid";
        i++;
    } else {
        document.getElementById('priceError').innerHTML = "";
    }

    if (!descriptionValidator.test(description)) {
        // console.log('description error');
        document.getElementById('descriptionError').innerHTML = "Description is invalid";
        i++;
    } else {
        document.getElementById('descriptionError').innerHTML = "";
    }

    if (!quantityValidator.test(quantity)) {
        // console.log('quantity error');
        document.getElementById('quantityError').innerHTML = "quantity is invalid";
        i++;
    } else {
        document.getElementById('quantityError').innerHTML = "";
    }

    if (!imageValidator.test(image['name'])) {
        // console.log('image error');
        document.getElementById('imageError').innerHTML = "Image is invalid";
        i++;
    } else {
        document.getElementById('imageError').innerHTML = "";
    }

    if (categories === undefined || categories.length === 0) {
        // console.log('category error');
        document.getElementById('categoryError').innerHTML = "Please choice a Category";
        i++;
    } else {
        document.getElementById('categoryError').innerHTML = "";
    }

    if (sizes === undefined || sizes.length === 0) {
        // console.log('size error');
        document.getElementById('sizeError').innerHTML = "Please choice a size";
        i++;
    } else {
        document.getElementById('sizeError').innerHTML = "";
    }

    if (colors === undefined || colors.length === 0) {
        // console.log('color error');
        document.getElementById('colorError').innerHTML = "Please choice a color";
        i++;
    } else {
        document.getElementById('colorError').innerHTML = "";
    }
    // console.log(i);
    return i === 0;
}

function addSizeForInsert() {
    sizes = [];
    var items = document.getElementsByName('sizeForInsert');
    for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox' && items[i].checked == true)
            sizes.push(items[i].value);
    }
}

function addColorForInsert() {
    colors = [];
    var items = document.getElementsByName('colorForInsert');
    for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox' && items[i].checked == true)
            colors.push(items[i].value);
    }
}

function addCategoryForInsert(id) {
    categories = [];
    var items = document.getElementsByName('categoryForInsert');
    for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox' && items[i].checked == true)
            categories.push(items[i].value);
    }
}

function createProduct() {
    name = document.getElementById('nameForInsert').value;
    price = document.getElementById('priceForInsert').value;
    description = document.getElementById('descriptionForInsert').value;
    quantity = document.getElementById('quantityForInsert').value;
    image = document.getElementById('imageForInsert').files[0];

    if (!validate()) {
        return false
    }
    let product = new FormData();
    product.append('productName', name);
    product.append('productPrice', price);
    product.append('productDescription', description);
    product.append('productQuantity', quantity);
    product.append('productImage', image);
    product.append('productCategories', categories);
    product.append('productColors', colors);
    product.append('productSizes', sizes);
    // let product = {
    //     productName         :   name,
    //     productPrice        :   price,
    //     productDescription  :   description,
    //     productQuantity     :   quantity,
    //     productImage        :   image,
    //     productCategories   :   categories,
    //     productColors       :   colors,
    //     productSizes        :   sizes
    // };
    xmlhttp = createXMLHttpRequestObject();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            document.getElementById("insertResult").innerHTML = this.responseText;
        }
    };
    xmlhttp.open("POST", "index.php?c=Product&a=doInsertProduct", true);
    // xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(product);

}