
function createXMLHttpRequestObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xmlhttp;
}

var colors = [];
var sizes = [];
var limit = [];
var currentPage = 1;

if(document.getElementById('category').value == 'malePants') {
    var categories = ['2','15'];
} else if (document.getElementById('category').value == 'menShirt') {
    var categories = ['16','29'];
}

function addLimitForFilter(startPoint, limitPoint, crPage) {
    console.log(startPoint, limitPoint);
    limit = [];
    currentPage = crPage;
    console.log(currentPage);
    limit.push(startPoint, limitPoint);
    filterProduct();
}

function addCategoryForFilter(left, right) {
    // console.info(left, right);
    limit = [];
    categories = [];
    categories.push(left, right);
    filterProduct();
}

function addSizeForFilter() {
    limit = [];
    sizes = [];
    var items = document.getElementsByName('sizeForFilter');
    for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox' && items[i].checked == true)
            sizes.push(items[i].value);
    }
    filterProduct();
}

function addColorForFilter() {
    limit = [];
    colors = [];
    var items = document.getElementsByName('colorForFilter');
    for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox' && items[i].checked == true)
            colors.push(items[i].value);
    }
    filterProduct();
}

function filterProduct() {
    let filter = {
        "category": categories,
        "color": colors,
        "size": sizes,
        "limit": limit,
        "currentPage" : currentPage
    };
    console.log(filter);
    xmlhttp = createXMLHttpRequestObject();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            // console.log("request end");
            document.getElementById("filterResult").innerHTML = this.responseText;
        }
    };
    xmlhttp.open("POST", "index.php?c=Product&a=getProductWithFilter", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify(filter));
}