<?php


namespace Module;


class UploadImage
{
    private $target_dir = "assets/img/upload/";
    private $error = array();

//    private $target_file = $target_dir.basename($_FILES['fileUpload']['name']);

    public function imageValidator($file)
    {
        $imageType = pathinfo($file['name'], PATHINFO_EXTENSION);
        $imageSize = $file['size'];
        $this->fileExtensionValidator($imageType);
        $this->fileSizeValidator($imageSize);
        if(empty($this->error)) {
            return true;
        } else {
            return $this->error;
        }
    }

    public function fileExtensionValidator($imageType)
    {
        $type_fileAllow = array('png', 'jpg', 'jpeg', 'gif');
        if (!in_array(strtolower($imageType), $type_fileAllow)) {
            $this->error[] = "Image file is not valid";
        }
    }

    public function fileSizeValidator($imageSIze)
    {
        if ($imageSIze > 5242880) {
            $this->error[] = "File upload not larger 5MB";
        }
    }

    public function doUploadImage($file) {
        $newName = $this->getNewNameFile($file['name']);
        $newPath = $this->target_dir . $newName;
        if (move_uploaded_file($file['tmp_name'], $newPath)) {
            return $newPath;
        } else {
            echo "upload file failed";
        }
        die;
    }


    function getNewNameFile($fileName)
    {
        $randomNum = time();
        $imageName = str_replace(' ', '-', strtolower($fileName));
        $imageExt = substr($imageName, strrpos($imageName, '.'));
        $imageExt = str_replace('.', '', $imageExt);
        $imageName = preg_replace("/\.[^.\s]{3,4}$/", "", $imageName);
        $newImageName = $imageName . '-' . $randomNum . '.' . $imageExt;

        return $newImageName;
    }


}