<?php


namespace Module;


class PaginationModule
{
    public function renderPagination($totalRecords, $limit, $currentPage = 1) {
        $totalPages = $totalRecords/$limit;
        $crPage = $currentPage;
        if($totalRecords%$limit != 0) {
            $totalPages += 1;
        }
        if($totalRecords == 0) {
            $data ='
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="javascript:void(0)">0</a></li>
                </ul>';
            return $data;
        }
        $data = "";
        $data.= "<ul class='pagination'>";
        for ($i = 1 ; $i <= $totalPages; $i++){
            $limitForFilter = $limit*($i-1) . "," . $limit;
            if($i == $crPage) {
                $data.= "<li class='page-item active'><a class='page-link ' onclick='addLimitForFilter(${limitForFilter},$i)'>$i</a></li>";
                continue;
            }
            $data.= "<li class='page-item'><a class='page-link' onclick='addLimitForFilter(${limitForFilter},$i)'>$i</a></li>";
        }
        $data.= "</ul>";
        return $data;
    }
}