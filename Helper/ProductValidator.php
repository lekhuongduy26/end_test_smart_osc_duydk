<?php
namespace Helper;
class ProductValidator
{
    public function nameValidate($name)
    {
        if (empty($name) || (!preg_match("/^[A-Za-z0-9_.\-\s]{3,}$/", $name))) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function priceValidate($price)
    {
        if (empty($price) || !preg_match('/[+-]?([0-9]*[.])?[0-9]+$/', $price) || $price < 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function quantityValidate($quantity)
    {
        if ($quantity > 999999 || $quantity < 0 || !preg_match('/^[0-9]+$/', $quantity)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function descriptionValidate($description)
    {
        if (empty($description) || (!preg_match("/^[A-Za-z0-9_.\-\s]{3,}$/", $description))) {
            return FALSE;
        } else {
            return TRUE;
        }
    }


    public function categoryValidate($categories)
    {
        if(empty($categories) && !explode(",",$categories)){
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function sizeValidate($sizes)
    {
        if(empty($sizes) && !explode(",",$sizes)){
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function colorValidate($colors)
    {
        if(empty($colors) && !explode(",",$colors)){
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
