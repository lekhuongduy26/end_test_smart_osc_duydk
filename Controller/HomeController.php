<?php
namespace Controller;
use Model;

class HomeController extends BaseController
{
    private $productModel = NULL;
    function __construct()
    {
        $this->productModel = new Model\ProductModel();
        $this->folder = "Product";
    }


    public function home() {
        $data = $this->productModel->getNewestProduct();

        $this->render("LatestProductView", $data);
    }

}
