<?php


namespace Controller;

use Model;

class ProductAttrController
{
    private $categoryModel = NULL;
    private $sizesModel = NULL;
    private $colorModel = NULL;

    private $menuLinkCategories = '';
    private $menuCheckboxCategories = '';

    function __construct()
    {
        $this->categoryModel = new Model\CategoryModel();
        $this->colorModel = new Model\ColorModel();
        $this->sizesModel = new Model\SizeModel();
    }

    public function getMainCategory($left, $right) {
        return $this->categoryModel->fetchByCondition("Where categories.lft >= ${left} and categories.rgt <= ${right}","*");
    }

    public function renderLinkMenuCategories($categories, $parent_id)
    {
        $cate_child = array();
        foreach ($categories as $key => $item)
        {
            if ($item['parent_id'] == $parent_id)
            {
                $cate_child[] = $item;
                unset($categories[$key]);
            }
        }
        if ($cate_child)
        {
            $this->menuLinkCategories .=
                '<ul style="list-style: none;font-size: 16px">';
            foreach ($cate_child as $key => $item)
            {
                $this->menuLinkCategories .=
            "<a href='javascript:void(0)' style='color:#c5322d' onclick='addCategoryForFilter(${item['lft']}, ${item['rgt']})'><li>${item['category']}</a>";
            $this->renderLinkMenuCategories($categories, $item['id']);
            $this->menuLinkCategories .= '</li>';
        }
            $this->menuLinkCategories .= '</ul>';
        }
    }

    public function renderCheckboxMenuCategories($categories, $parent_id)
    {
        $cate_child = array();
        foreach ($categories as $key => $item)
        {
            if ($item['parent_id'] == $parent_id)
            {
                $cate_child[] = $item;
                unset($categories[$key]);
            }
        }
        if ($cate_child)
        {
            $this->menuCheckboxCategories .=
                '<ul style="list-style: none;">';
            foreach ($cate_child as $key => $item)
            {
                $this->menuCheckboxCategories .=
                            "<div class='form-check'>
                                <label class='form-check-label'>
                                    <input type='checkbox' name='categoryForInsert' id='categoryForInsert' onclick='addCategoryForInsert()' value='${item['id']}'>&ensp;${item['category']}
                                </label>
                            </div>
                            ";

                $this->renderCheckboxMenuCategories($categories, $item['id']);
                $this->menuCheckboxCategories .= '</li>';
            }
            $this->menuCheckboxCategories .= '</ul>';
        }
    }

    public function getLinkMenuCategories($category)
    {
        $this->menuLinkCategories = '';
        if($category == 'malePants') {
            $parent_id = '1';
            $left = '2';
            $right = '15';
        } elseif ($category == "menShirt") {
            $parent_id = '1';
            $left = '16';
            $right = '29';
        }
        $categories = $this->getMainCategory($left, $right);
        $this->renderLinkMenuCategories($categories, $parent_id);
        $this->menuLinkCategories.= "<input type='hidden' id='category' value='${category}'>";
        return $this->menuLinkCategories;
    }

    public function getCheckboxMenuCategories()
    {
        $categories = $this->getAllCategory();
        $this->renderCheckboxMenuCategories($categories, 1);;
        return $this->menuCheckboxCategories;
    }

    public function getAllColor() {
        return $this->colorModel->fetchAll("*");
    }

    public function getAllSize() {
        return $this->sizesModel->fetchAll("*");
    }
    
    public function getAllCategory() {
        return $this->categoryModel->fetchAll("*");
    }


}