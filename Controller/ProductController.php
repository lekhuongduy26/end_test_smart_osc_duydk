<?php

namespace Controller;

use Model;
use Controller;
use Module;
use Helper;

class ProductController extends BaseController
{
    private $productModel = NULL;
    private $paginationModule = NULL;
    private $filterController = NULL;
    private $uploadImageModule = Null;
    private $productCategoryModel = NULL;
    private $productSizeModel = NULL;
    private $productColorModel = NULL;
    private $productValidator = NULL;

    function __construct()
    {
        $this->productModel = new Model\ProductModel();
        $this->filterController = new Controller\ProductAttrController();
        $this->filterController = new Controller\ProductAttrController();
        $this->paginationModule = new Module\paginationModule();
        $this->uploadImageModule = new Module\UploadImage();
        $this->productCategoryModel = new Model\ProductCategoryModel();
        $this->productSizeModel = new Model\ProductSizeModel();
        $this->productColorModel = new Model\ProductColorModel();
        $this->productValidator = new Helper\ProductValidator();
        $this->folder = "Product";
    }


    function getShopPage($left = NULL, $right = NULL)
    {
        $category = !empty($_GET['cate']) ? $_GET['cate'] : "";
        if ($category == 'malePants') {
            $left = '2';
            $right = '15';
        } elseif ($category == "menShirt") {
            $left = '16';
            $right = '29';
        }
        $data = array();
        $data['category'] = $this->filterController->getLinkMenuCategories($category);
        $data['color'] = $this->filterController->getAllColor();
        $data['size'] = $this->filterController->getAllSize();
        $data['product'] = $this->productModel->getProductWithFilter($left, $right,'','',array('0','9') );
        $totalRecords = $this->productModel->getProductWithFilter($left, $right,'','' );
        $data['pagination'] = $this->paginationModule->renderPagination($totalRecords, 9, 1);
        $this->render("ProductView", $data);
    }

    function getProductWithFilter()
    {
        $data = file_get_contents("php://input");
        $filters = (array)json_decode($data);
        $currentPage = !empty($filters['currentPage']) ? $filters['currentPage'] : 1;
        if (!empty($filters['category'][0]) && !empty($filters['category'][1])) {
            $left = $filters['category'][0];
            $right = $filters['category'][1];
        }
        $limit = !empty($filters['limit']) ? $filters['limit'] : array('0','9');
        $productFilterResult = $this->productModel->getProductWithFilter($left, $right, $filters['color'], $filters['size'], $limit);
        $totalRecords = $this->productModel->getProductWithFilter($left, $right, $filters['color'], $filters['size']);
        $pagination = $this->paginationModule->renderPagination($totalRecords, 9, $currentPage);

        require_once "View/Product/productFilterResult.php";
    }

    public function validateFormProduct($data)
    {
        $error = array();
        if (!$this->productValidator->nameValidate($data['productName'])) {
            $error[] = "NAME IS INVALID";
        }
        if (!$this->productValidator->priceValidate($data['productPrice'])) {
            $error[] = "PRICE IS INVALID";
        }
        if (!$this->productValidator->quantityValidate($data['productQuantity'])) {
            $error[] = "QUANTITY IS INVALID";
        }
        if (!$this->productValidator->descriptionValidate($data['productDescription'])) {
            $error[] = "DESCRIPTION IS INVALID";
        }
        if (!$this->productValidator->categoryValidate($data['productCategories'])) {
            $error[] = "CATEGORIES IS INVALID";
        }
        if (!$this->productValidator->sizeValidate($data['productSizes'])) {
            $error[] = "SIZES IS INVALID";
        }
        if (!$this->productValidator->colorValidate($data['productColors'])) {
            $error[] = "COLORS IS INVALID";
        }
        return $error;
    }

    function doInsertProduct() {
        $product = [];
        $image_url = NULL;
        $error = $this->validateFormProduct($_POST);
        if($this->uploadImageModule->imageValidator($_FILES['productImage'])) {
            $image_url = $this->uploadImageModule->doUploadImage($_FILES['productImage']);
        } else {
            $error = "Image is invalid";
        }
        if(!empty($error)) {
            $notification['status'] = 'danger';
            $notification['message'] = $error;
            require_once "View/Product/notification.php";
            return;
        }

        $product = [
            'name' => $_POST['productName'],
            'price' => $_POST['productPrice'],
            'description' => $_POST['productDescription'],
            'quantity' => $_POST['productQuantity'],
            'image_url' => $image_url
        ];
        $lastInsertId = $this->productModel->save($product);

        $productsCategories = [];
        foreach (explode(",",$_POST['productCategories']) as $value) {
            $productsCategories[] = array($value,$lastInsertId);
        }
        $this->productCategoryModel->save($productsCategories);

        $productsColor = [];
        foreach (explode(",",$_POST['productColors']) as $value) {
            $productsColor[] = array($value,$lastInsertId);
        }
        $this->productColorModel->save($productsColor);

        $productsSizes = [];
        foreach (explode(",",$_POST['productSizes']) as $value) {
            $productsSizes[] = array($value,$lastInsertId);
        }
        $this->productSizeModel->save($productsSizes);
        $notification['status'] = 'success';
        $notification['message'] = "Insert Successfully";
        require_once "View/Product/notification.php";
    }

    public function getInsertProductForm()
    {
        $categories = $this->filterController->getAllCategory();
        $data['category'] = $this->filterController->getCheckboxMenuCategories();
        $data['color'] = $this->filterController->getAllColor();
        $data['size'] = $this->filterController->getAllSize();
        $this->render('AddProductForm', $data);
    }


}
