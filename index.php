<?php
require_once "config.php";
$autoLoad = function ($className) {
    $path = str_replace('\\','/', __DIR__ . '/' .$className . '.php');
    require_once $path;
};

$controller = $_GET["c"] ?? "Home";
$action = $_GET["a"] ?? "home";
spl_autoload_register($autoLoad);
$controllerNameSpace = '\Controller\\'. $controller . 'Controller';
$controller = new $controllerNameSpace();
$controller->$action();
