<?php


namespace Model;

use Model\BaseModel;

class ProductColorModel extends BaseModel
{
    protected $tableName = 'product_color';

    protected $columns = ['color_id', 'product_id'];
}