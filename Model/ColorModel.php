<?php


namespace Model;


class ColorModel extends BaseModel
{
    protected $tableName = 'colors';

    protected $primaryKey = "id";

    protected $columns = ['color'];
}