<?php

namespace Model;
use Connection\Connection;
class DatabaseModel
{
    protected $connection = NULL;

    public function __construct()
    {
        $instance = Connection::getInstance();
//        var_dump($instance);die;
        $this->connection = $instance->getConnection();
//        var_dump($this->connection);die;
    }

    /**
     * @param $tableName
     * @param $column
     * @param null $condition
     * @return mixed
     */
    public function fetch($tableName, $column = '*', $condition = NULL)
    {
        $sql = "SELECT DISTINCT ${column} ";
        $sql .= "FROM $tableName ";
        if (!empty($condition)) {
            $sql .= "${condition}";
//            var_dump($sql);
            $data = $this->connection->query($sql);
            $result = [];
            while ($line = $data->fetch_assoc()) {
                $result[] = $line;
            }
            if(empty($result)) {
                return "NOT FOUND";
            }
            return $result;
        }
        $data = $this->connection->query($sql);
        $result = [];
        while ($line = $data->fetch_assoc()) {
            $result[] = $line;
        }
        return $result;
    }

    public function insert($tableName, $column, $values)
    {
//        $values = $this->connection->real_escape_string($values);
//        var_dump($values);die;
        $sql = "INSERT INTO ${tableName} (${column}) ";
        $sql .="VALUES ${values}";
//        var_dump($sql);
        if (!($stmt = $this->connection->prepare($sql))) {
            echo "Prepare failed: (" . $this->connection->errno . ") " . $this->connection->error;
        }
//        echo "<pre>";
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        } else {
            return $this->connection->insert_id;
        }
    }

    public function update($tableName, $data, $condition)
    {
        $sql = "UPDATE ${tableName} ";
        $sql .= "SET ${data} ";
        $sql .= "WHERE ${condition}";
        if (!($stmt = $this->connection->prepare($sql))) {
            echo "Prepare failed: (" . $this->connection->errno . ") " . $this->connection->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        } else {
            return true;
        }
    }

    public function delete($tableName, $primary_key, $ids)
    {
        if(empty($ids)) return FALSE;
        if (is_array($ids)) {
            $limit = count($ids);
        } else {
            $limit = 1;
        }
        $sql = "DELETE FROM ${tableName} WHERE ${primary_key} IN ($ids) LIMIT ${limit}";
        if (!($stmt = $this->connection->prepare($sql))) {
            echo "Prepare failed: (" . $this->connection->errno . ") " . $this->connection->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        } else {
            return true;
        }
    }

}