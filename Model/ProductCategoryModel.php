<?php


namespace Model;

use Model\BaseModel;

class ProductCategoryModel extends BaseModel
{
    protected $tableName = 'product_category';

    protected $columns = ['category_id', 'product_id'];

}