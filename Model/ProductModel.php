<?php

namespace Model;

use Model\BaseModel;



class ProductModel extends BaseModel
{
    protected $tableName = 'Products';

    protected $primaryKey = "id";

    protected $columns = ['name', 'price', 'description', 'quantity', 'image_url'];

    public function getProductWithFilter($left = 1, $right = 30, $color = array(), $size = array(), $limit = array())
    {
        $tbProductWithJoin = $this->tableName . " AS p 
                            JOIN product_category AS pc ON p.id = pc.product_id
                            JOIN categories AS c ON pc.category_id = c.id ";
        $condition = "WHERE (c.lft >= '${left}' AND c.rgt <= '${right}') ";
        if (!empty($color)) {
            $color = "'" . implode("','", $color) . "'";
            $tbProductWithJoin .= "JOIN product_color AS pcolor ON pcolor.product_id = p.id
                                   JOIN colors ON pcolor.color_id = colors.id ";
            $condition         .= "AND colors.id IN (${color}) ";
        }
        if(!empty($size)) {
            $size = "'" . implode("','", $size) . "'";
            $tbProductWithJoin .= "JOIN product_size AS psize ON psize.product_id = p.id
                                   JOIN sizes ON psize.size_id = sizes.id ";
            $condition         .= "AND sizes.id IN (${size}) ";
        }
        if(!empty($limit)) {
            $condition .= " LIMIT ${limit['0']}, ${limit['1']}";
        } else {
            //neu khong co limit thi lay tong so ban ghi cho phan trang
            $result = $this->DbModel->fetch($tbProductWithJoin, "COUNT(DISTINCT p.id) AS total", $condition);
            return $totalRecord = $result['0']['total'];
        }
        return $this->DbModel->fetch($tbProductWithJoin, $this->getStringColumns(), $condition);
    }

    public function getNewestProduct()
    {
        $condition = "ORDER BY insert_Time DESC LIMIT 8";
        return $this->fetchByCondition($condition);
    }

    public function setCategories($lastInsertId, $categories) {

    }
}
