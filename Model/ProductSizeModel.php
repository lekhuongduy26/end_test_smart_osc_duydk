<?php


namespace Model;

use Model\BaseModel;

class ProductSizeModel extends BaseModel
{
    protected $tableName = 'product_size';

    protected $columns = ['size_id', 'product_id'];
}