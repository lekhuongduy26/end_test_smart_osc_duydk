<?php

namespace Model;

use Model\DatabaseModel;

class BaseModel
{

    protected $DbModel;

    protected $tableName = NULL;

    protected $primaryKey = NULL;

    protected $columns = array();

//    protected $data = [];

    public function __construct()
    {
        $this->DbModel = new DatabaseModel();
    }


    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function getStringColumns()
    {
        return implode(",", $this->columns);
    }


    function fetchAll($columns = NULL)
    {
        if (empty($columns)) {
            $columns = $this->getStringColumns();
        }
        return $this->DbModel->fetch($this->getTableName(), $columns);
    }

    function fetchByCondition($condition, $columns = NULL)
    {
        if (empty($columns)) {
            $columns = $this->getStringColumns();
        }
        return $this->DbModel->fetch($this->getTableName(), $columns, $condition);
    }

    /**
     * @param $primaryKey
     * @param string $columns
     * @return mixed
     */
    public function load($primaryKey, $columns = NULL)
    {
        if (empty($columns)) {
            $columns = $this->getStringColumns();
        }
        if (empty($primaryKey)) return FALSE;
        $condition = "WHERE " . $this->getPrimaryKey() . " = ${primaryKey}";
        $result = $this->DbModel->fetch($this->getTableName(), $columns, $condition);
        return $result;
    }

    public function save($data, $primaryKey = NULL)
    {
        if ($this->isUpdating($primaryKey)) {
            $data = $this->createUpdateQuery($data);
            $condition = $this->getPrimaryKey() . " = ${primaryKey}";
            $result = $this->DbModel->update($this->getTableName(), $data, $condition);
            return $result;
        } else {
            $data = $this->createInsertQuery($data);
            $lastInsertId = $this->DbModel->insert($this->getTableName(), $this->getStringColumns(), $data);
            return $lastInsertId;
        }
    }

    public function delete($id)
    {
        if (is_array($id)) {
            $id = "'" . implode("','", $id) . "',";
            $id = trim($id, ',');
        }
        $result = $this->DbModel->delete($this->getTableName(), $this->getPrimaryKey(), $id);
        return $result;
    }

    /**
     * @param $data
     * @return string
     */
//    protected function prepareDataBeforeSave($data)
//    {
////        echo "<pre/>";
////        var_dump($data);die;
//        foreach ($this->getColumns() as $value) {
//            $this->setData("${value}", "${data["${value}"]}");
//        }
////        $value = "'" . implode("','", $data) . "',";
//        return $this;
//    }

    protected function createUpdateQuery($data)
    {
        $query = '';
        foreach ($data as $key => $value) {
            $query .= $key . "= '${value}',";
        }
        $query = trim($query, ',');
        return $query;
    }

    protected function createInsertQuery($data)
    {
        $query = '';
        if($this->is_multi_array($data)) {
            foreach ($data as $key => $value) {
                $query .= "('" . implode("','", $value) . "'),";
            }
            $query = trim($query, ',');
            return $query;
        }
        $query = "('" . implode("','", $data) . "'),";
        $query = trim($query, ',');
        return $query;
    }

    function is_multi_array( $arr ) {
        rsort( $arr );
        return isset( $arr[0] ) && is_array( $arr[0] );
    }


    /**
     * @param $id
     * @return bool
     */
    public function isUpdating($id)
    {
        if (!empty($id)) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
//    public function setData($key, $value)
//    {
//        $this->data[$key] = $value;
//        return $this;
//    }
//
//    /**
//     * @param $key
//     * @return string
//     */
//    public function getData($key)
//    {
//        return array_key_exists($key, $this->data) ? $this->data[$key] : FALSE;
//    }
}