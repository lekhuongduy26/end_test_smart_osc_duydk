<?php


namespace Model;


class SizeModel extends BaseModel
{
    protected $tableName = 'sizes';

    protected $primaryKey = "id";

    protected $columns = ['size'];
}