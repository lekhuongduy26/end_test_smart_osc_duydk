<?php


namespace Model;


class CategoryModel extends BaseModel
{
    protected $tableName = 'categories';

    protected $primaryKey = "id";

    protected $columns = ['category','lft','rgt','parent_id'];
}