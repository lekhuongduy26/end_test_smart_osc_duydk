<section class="home_banner_area">
    <div class="banner_inner d-flex align-items-center" >
        <div class="container">
            <div class="banner_content row">
                <div class="col-lg-4">
                    <h3>Georgia Helmet <br />Collections!</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                    <a class="white_bg_btn" href="#">View Collection</a>
                </div>
                <div class="col-lg-8">
                    <div class="halemet_img">
                        <img style="padding-right: 50px" src="assets/img/banner/bannerLuffy.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>