<div class="product_top_bar">
    <div class="right_page ml-auto">
        <nav class="cat_page" aria-label="Page navigation example">
            <?= $pagination ?>
        </nav>
    </div>
</div>
<?php if(!is_array($productFilterResult)): ?>
    <div class="alert alert-danger" role="alert">
        <strong>PRODUCT <?= $productFilterResult ?></strong>
    </div>
<?php endif; ?>
<?php if(is_array($productFilterResult)): ?>
<div class="latest_product_inner row">
<?php foreach ($productFilterResult as $value): ?>
    <div class="col-lg-4 col-md-4 col-sm-6">
        <div class="f_p_item">
            <div class="f_p_img">
                <img class="img-fluid" src="<?= $value["image_url"] ?>" alt="">
                <div class="p_icon">
                    <a href="#"><i class="lnr lnr-heart"></i></a>
                    <a href="#"><i class="lnr lnr-cart"></i></a>
                </div>
            </div>
            <a href="#"><h4><?= $value["name"]; ?></h4></a>
            <h5><?= $value["price"]; ?></h5>
        </div>
    </div>
<?php endforeach; ?>
</div>
<?php endif; ?>
