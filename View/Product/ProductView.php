<section class="cat_product_area p_120">
    <div class="container">
        <div class="row flex-row-reverse">
            <div id="filterResult" class="col-lg-9">
                <div class="product_top_bar">
                    <div class="right_page ml-auto">
                        <nav class="cat_page" aria-label="Page navigation example">
                            <?= $data['pagination']; ?>
                        </nav>
                    </div>
                </div>
                <div class="latest_product_inner row">
                    <?php foreach ($data['product'] as $value): ?>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="f_p_item">
                                <div class="f_p_img">
                                    <img class="img-fluid" src="<?= $value["image_url"] ?>" alt="">
                                    <div class="p_icon">
                                        <a href="javascript:void(0)"><i class="lnr lnr-heart"></i></a>
                                        <a href="javascript:void(0)"><i class="lnr lnr-cart"></i></a>
                                    </div>
                                </div>
                                <a href="javascript:void(0)"><h4><?= $value["name"]; ?></h4></a>
                                <h5><?= $value["price"]; ?></h5>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="left_sidebar_area">
                    <aside class="left_widgets cat_widgets">
                        <div class="l_w_title">
                            <h3>Browse Categories</h3>
                        </div>
                        <div id="categories" class="widgets_inner_custom">
                            <?= $data["category"]; ?>
                        </div>
                    </aside>
                    <aside class="left_widgets p_filter_widgets">
                        <div class="l_w_title">
                            <h3>Product Filters</h3>
                        </div>
                        <div class="widgets_inner">
                            <h4>Color</h4>
<!--                            <ul class="list">-->
                            <div style="padding-left: 30px;font-size: 16px">
                                <?php foreach ($data['color'] as $color): ?>
                                            <div class="chiller_cb">
<!--                                                <input id="myCheckbox" type="checkbox" checked>-->
                                                <input id="colorId<?= $color['id']?>" type='checkbox' name='colorForFilter' onclick='addColorForFilter()' value='<?= $color['id']?>' >
                                                <label for="colorId<?= $color['id']?>"><?= $color['color'] ?></label>
                                                <span></span>
                                            </div>
<!--                                    <li>-->
<!--                                        <label class='form-check-label'>-->
<!--                                            <input type='checkbox' name='colorForFilter' onclick='addColorForFilter()' value='--><?//= $color['id']?><!--' >--><?//= $color['color'] ?>
<!--                                        </label>-->
<!--                                    </li>-->

                                <?php endforeach; ?>
                            </div>
<!--                            </ul>-->
                        </div>
                        <div class="widgets_inner">
                            <h4>Size</h4>
<!--                            <ul class="list">-->
                            <div style="padding-left: 30px; font-size: 16px">
                                <?php foreach ($data['size'] as $size): ?>
                                    <div class="chiller_cb">
                                        <!--                                                <input id="myCheckbox" type="checkbox" checked>-->
                                        <input id="sizeId<?= $size['id'] ?>" type='checkbox' name='sizeForFilter' onclick='addSizeForFilter()' value='<?= $size['id']?>' >
                                        <label for="sizeId<?= $size['id'] ?>"><?= $size['size'] ?></label>
                                        <span></span>
                                    </div>
<!--                                    <li>-->
<!--                                        <label class='form-check-label'>-->
<!--                                            <input type='checkbox' name='sizeForFilter' onclick='addSizeForFilter()' value='--><?//= $size['id']?><!--' >--><?//= $size['size'] ?>
<!--                                        </label>-->
<!--                                    </li>-->
                                <?php endforeach; ?>
                            </div>
<!--                            </ul>-->
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="assets/js/filterCategoryAjax.js"></script>