<div class="alert alert-<?= $notification['status'] ?> alert-dismissible">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>
        <?php
        if(is_array($notification['message'])){
            foreach ($notification['message'] as $value) {
                echo $value , "<br/>";
            }
        } else {
            echo $notification['message'];
        }
        ?>
    </strong>
</div>