<div class="main_box" style="padding-top: 50px">
    <form enctype="multipart/form-data" id="createNewProductForm" action="javascript:void(0)" onsubmit="createProduct()">
        <div class="row">
            <div class="col-md-4 col-sm-12 offset-md-1">
                <div class="form-group">
                    <label for="nameForInsert">Name</label>
                    <input type="text" class="form-control" id="nameForInsert" name="nameForInsert" placeholder="Enter Product Name" required>
                <p id="nameError" style="color: red;text-align: center"></p>
                </div>
<!--                name-->
                <div class="form-group">
                    <label for="priceForInsert">Price</label>
                    <input type="number" class="form-control" id="priceForInsert" name="priceForInsert" step="0.001" min="0"
                           max="999999999"
                           placeholder="Enter price" value="1" required>
                    <p id="priceError" style="color: red;text-align: center"></p>
                </div>
<!--                price-->
                <div class="form-group">
                    <label for="descriptionForInsert">Description</label>
                    <input type="text" class="form-control" id="descriptionForInsert" name="descriptionForInsert" placeholder="Enter description" required>
                    <p id="descriptionError" style="color: red;text-align: center"></p>
                </div>
<!--                    description-->
                <div class="form-group">
                    <label for="quantityForInsert">Quantity</label>
                    <input type="number" class="form-control" id="quantityForInsert" name="quantityForInsert" min="1" step="1"
                           max="999999"
                           required placeholder="Enter quantity" value="1">
                    <p id="quantityError" style="color: red;text-align: center"></p>
                </div>
<!--                    quantity-->
                <div class="form-group">
                    <label for="imageForInsert">Image</label>
                    <input type="file" class="form-control" id="imageForInsert" name="imageForInsert" accept="image/png, image/gif, image/jpeg, image/jpg" required>
                    <p id="imageError" style="color: red;text-align: center"></p>
                </div>
<!--                image-->
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <h3 style="text-align: center">Category</h3>
                        <?= $data['category'] ?>
                        <p style="color: red" id="categoryError"></p>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-md-12" style="text-align: center">
                                <h3>Size</h3>
                                <?php foreach ($data['size'] as $value): ?>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" id="sizeForInsert" name="sizeForInsert" class="form-check-input" onclick="addSizeForInsert()"
                                                   value="<?= $value['id'] ?>"><?= $value['size'] ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                                <p style="color: red" id="sizeError"></p>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-12" style="text-align: center">
                                <h3>Color</h3>
                                <?php foreach ($data['color'] as $value): ?>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" id="colorForInsert" name="colorForInsert" class="form-check-input" onclick="addColorForInsert()"
                                                   value="<?= $value['id'] ?>"><?= $value['color'] ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                                <p style="color: red" id="colorError"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <p style="text-align: center">
            <button type="reset" class="btn btn-warning" onclick="resetFrom()">Reset</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </p>
    </form>
    <div id="insertResult"></div>
</div>
<script src="assets/js/createProductAjax.js"></script>